//
//  main.m
//  Socket-请求百度
//
//  Created by 梁森 on 2018/5/20.
//  Copyright © 2018年 梁森. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
